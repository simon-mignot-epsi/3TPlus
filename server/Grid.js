
module.exports = function()
{
    return {
        Grid: Grid,
        at: at,
        updateMargin: updateMargin,
        cellExists: cellExists,
        getWidth: getWidth,
        getHeight: getHeight
    };
};


function at(grid, x, y)
{
    return cellExists(grid, x, y) ? grid[y][x] : null;
}

function Grid()
{
    return [[-1, -1, -1], [-1, -1, -1], [-1, -1, -1]];
}

function updateMargin(grid)
{
    let change = {};
    change.top = updateMarginTop(grid) | 0;
    change.bottom = updateMarginBottom(grid) | 0;
    change.left = updateMarginLeft(grid) | 0;
    change.right = updateMarginRight(grid) | 0;
    
    return change;
}


function updateMarginTop(grid)
{
    let w = getWidth(grid);
    for(let i = 0; i < w; ++i)
    {
        let j;
        for(j = 0; j < 3; ++j)
            if(cellExists(grid, i, j) && grid[j][i] !== -2)
                break;
        
        const toAdd = 3 - j;
        if(toAdd === 0)
            continue;
        addRowsStart(grid, 3 - j);
        return toAdd;
    }
}
function updateMarginBottom(grid)
{
    let w = getWidth(grid),
        h = grid.length;
    for(let i = 0; i < w; ++i)
    {
        let j;
        for(j = 0; j < 3; ++j)
            if(cellExists(grid, i, h - j - 1) && grid[h - j - 1][i] !== -2)
                break;
        
        const toAdd = 3 - j;
        if(toAdd === 0)
            continue;
        addRowsEnd(grid, toAdd);
        return toAdd;
    }
}
function updateMarginLeft(grid)
{
    let h = getHeight(grid);
    for(let j = 0; j < h; ++j)
    {
        let i = 0;
        for(i = 0; i < 3; ++i)
            if(cellExists(grid, i, j) && grid[j][i] !== -2)
                break;
        
        const toAdd = 3 - i;
        if(toAdd === 0)
            continue;
        addColsStart(grid, toAdd);
        return toAdd;
    }
}
function updateMarginRight(grid)
{
    let h = getHeight(grid);
    for(let j = 0; j < h; ++j)
    {
        let i, w = grid[0].length;
        for(i = w - 1; i > w - 4; --i)
            if(cellExists(grid, i, j) && grid[j][i] !== -2)
                break;
        
        const toAdd = 3 - (w - 1 - i);
        if(toAdd === 0)
            continue;
        addColsEnd(grid, toAdd);
        return toAdd;
    }
}

function addRowsStart(grid, nb)
{
    let row = [];
    for(let i = 0; i < getWidth(grid); ++i)
        row.push(-2);
    for(let i = 0; i < nb; ++i)
        grid.unshift(row.slice());
}
function addRowsEnd(grid, nb)
{
    let row = [];
    for(let i = 0; i < getWidth(grid); ++i)
        row.push(-2);
    for(let i = 0; i < nb; ++i)
        grid.push(row.slice());
}
function addColsStart(grid, nb)
{
    let h = getHeight(grid);
    for(let j = 0; j < h; ++j)
        for(let c = 0; c < nb; ++c)
            grid[j].unshift(-2);
}
function addColsEnd(grid, nb)
{
    let h = getHeight(grid);
    for(let j = 0; j < h; ++j)
        for(let c = 0; c < nb; ++c)
            grid[j].push(-2);
}

function cellExists(grid, x, y)
{
    return typeof(grid[y]) === 'object' && typeof (grid[y][x]) === 'number';
}

function getHeight(grid)
{
    return grid.length;
}
function getWidth(grid)
{
    return grid[0].length;
}